# Relic Calculator

This program was written to calculate the chances of getting certain rewards when using X amount of relics


## Installation

Head over to: [The Go Programming Language](https://go.dev).  
Select **Download**, and select whatever platform you intend to install on.  
Once installed, you can run the command below in your terminal.  

```
go install gitlab.com/papochun/relic@latest
```


## Contact

You can contact me via this email:  
<papochun.unnamed123@slmail.me>
