package main

import (
	"fmt"
	"math"
	"strconv"
	"log"
)

type relic struct {
	common float64
	uncommon float64
	rare float64
}

var intact = relic{
	common: 0.2533,
	uncommon: 0.11,
	rare: 0.02,
}
var exceptional = relic{
	common: 0.2333,
	uncommon: 0.13,
	rare: 0.04,
}
var flawless = relic{
	common: 0.2,
	uncommon: 0.17,
	rare: 0.06,
}
var radiant = relic{
	common: 0.1667,
	uncommon: 0.2,
	rare: 0.1,
}

// asks for the relic quality and reward tier to calculate the reward chance per 1 relic
// given strings 'intact', and 'common' -> chance = intact.common ->  chance = 0.2533
func getChance() (float64) {
	var quality string
	fmt.Println("Intact [1] ")
	fmt.Println("Exceptional [2] ")
	fmt.Println("Flawless [3] ")
	fmt.Println("Radiant [4] ")
	fmt.Printf("Relic Quality: ")
	fmt.Scanln(&quality)

	var reward string
	fmt.Println("Common [1] ")
	fmt.Println("Uncommon [2] ")
	fmt.Println("Rare [3] ")
	fmt.Printf("Relic Reward Tier: ")
	fmt.Scanln(&reward)

	// define a pointer for the relic, which we point to whatever quality is selected
	var relic_pointer *relic

	switch quality {
	case "1":
		relic_pointer = &intact
	case "2":
		relic_pointer = &exceptional
	case "3":
		relic_pointer = &flawless
	case "4":
		relic_pointer = &radiant
	default: 
		relic_pointer = &intact
	}

	// define variable 'chance' which will use the value assigned from the reward tier
	var chance float64

	switch reward {
	case "1":
		chance = relic_pointer.common
	case "2":
		chance = relic_pointer.uncommon
	case "3":
		chance = relic_pointer.rare
	default: 
		log.Fatal()
	}

	return chance
}

// ask for relic count and converts to type 'float64' for convenience and readability
func getRelicCount() (float64) {
	fmt.Printf("Relic Count: ")
	var buffer string = ""
	fmt.Scanln(&buffer)

	count, err := strconv.ParseFloat(buffer, 64)
	if err != nil {
		log.Fatal(err)
	}
	return count
}

// given the chance of failure and the count of relics
// return the total drop chance of 'n' relics
func chancePerRelic(chance_fail float64, count float64) (float64) {
	failure := math.Pow(chance_fail, count)
	success := float64(1) - failure
	return float64(100) * success
}

// given the chance of failure and the desired success rate
// calculate and return how many relics are required to meet success rate
func relicPerChance(chance_fail float64, success float64) (float64) {
	var total_chance float64
	var count float64
	for count = float64(0); total_chance <= success; count++ {
		total_chance = 0
		total_chance = chancePerRelic(chance_fail, count)
	}
	return count
}

func main() {
	chance := getChance()
	chance_fail := float64(1) - chance
	count := getRelicCount()

	var total_chance float64
	var required_relics float64

	// print the required relics to attain a 90% chance
	required_relics = relicPerChance(chance_fail, 50)
	fmt.Printf("%0.f relics for at least 50%%\n", required_relics)

	// print the required relics to attain a 95% chance
	required_relics = relicPerChance(chance_fail, 75)
	fmt.Printf("%0.f relics for at least 75%%\n", required_relics)

	// print the required relics to attain a 99% chance
	required_relics = relicPerChance(chance_fail, 90)
	fmt.Printf("%0.f relics for at least 90%%\n", required_relics)

	// print the percent chance of the desired reward given 'n' relics
	total_chance = chancePerRelic(chance_fail, count)
	fmt.Printf("%0.f relics for at least %.2f%%\n", count, total_chance)
}
